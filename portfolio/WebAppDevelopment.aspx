﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebAppDevelopment.aspx.cs" Inherits="portfolio.WebAppDevelopment" %>
<asp:Content ID="testcontainer" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="page-heading"><a href="WebAppDevelopment.aspx" class="head-title">Web Application Development</a></h1>
    <h2>ASP.NET Application Life Cycle </h2>
    <ul>
        <li>User makes a request for accessing application resource, a page. Browser sends this request to the web server.</li>
        <li>A unified pipeline receives the first request and the following events take place:</li>
        <ul>
            <li>An object of the class ApplicationManager is created.</li>
            <li>An object of the class HostingEnvironment is created to provide information regarding the resources.</li>
            <li>Top level items in the application are compiled.</li>
        </ul>
        <li>Response objects are created. The application objects such as HttpContext, HttpRequest and HttpResponse are created and initialized.</li>
        <li>An instance of the HttpApplication object is created and assigned to the request.</li>
        <li>The request is processed by the HttpApplication class. Different events are raised by this class for processing the request.</li>
    </ul>
</asp:Content>
<asp:Content ContentPlaceHolderID="CodePart1" runat="server">
    <h2>Code Snippet</h2>
    <p style="font-family: 'PT Serif', serif;">
        <pre>
        &lt;form id="form1" runat="server"&gt;
            &lt;div&gt;
            &lt;asp:Calendar ID="Calendar1" runat="server" OnSelectionChanged="Calendar1_SelectionChanged"&gt;
            &lt;/asp:Calendar&gt;
            &lt;asp:TextBox ID="TextBox1" runat="server"&gt;
            &lt;/asp:TextBox&gt;
            &lt;/div&gt;
        &lt;/form&gt;
            </pre>
   </p>
</asp:Content>
<asp:Content ContentPlaceHolderID="CodePart2" runat="server">
<h2>Code Example</h2>
    <p style="font-family: 'PT Serif', serif;"></p>
    <pre>
&lt;div class="html"&gt;
    &lt;html&gt;<!--TO GET HTML CODE ON HTML PAGE REPLACE < with &lt; and > with &gt;-->
    &lt;head&gt;&lt;title&gt;Title&lt;/title&gt;&lt;/head&gt;
    &lt;body&gt;
    &lt;p&gt;Unrendred html&lt;/p&gt;
    &lt;/body&gt;
    &lt;/html&gt;
&lt;/div&gt;
</pre>
</asp:Content>
<asp:Content ContentPlaceHolderID="CodePart3" runat="server">
    <h2>Useful Links</h2>
    <div>
        <ul class="links">
            <li><a href="https://stackoverflow.com/questions/6382669/calendar-control-in-asp-net-c-sharp">Calender Control</a></li>
            <li><a href="https://www.c-sharpcorner.com/UploadFile/93126e/some-useful-and-important-concept-of-C-Sharp/">Important Concepts</a></li>
            <li><a href="https://www.quora.com/How-do-I-Display-HTML-code-on-Web-Page">Display Html code on HTML Page.</a></li>
        </ul>
    </div>
</asp:Content>
