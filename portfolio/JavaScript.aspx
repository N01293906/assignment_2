﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JavaScript.aspx.cs" Inherits="portfolio.JavaScript" %>
<asp:Content ID="testcontainer" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="page-heading"><a href="JavaScript.aspx" class="head-title">Javascript</a></h1>
    <h2>Tips </h2>
    <ul>
        <li>use === instead of ==</li>
        <li>Be careful when using typeof, instanceof and constructor.</li>
        <ul>
            <li>typeof : a JavaScript unary operator used to  return a string that represents the primitive type of a variable,  don’t forget that typeof null will return “object”, and for the majority of object types (Array, Date, and others) will return also “object”.</li>
            <li>constructor : is a property of the internal prototype property, which could be overridden by code.</li>
            <li>instanceof : is another JavaScript operator that check in all the prototypes chain the constructor it returns true if it’s found and false if not.</li>
        </ul>
        <li>Avoid negative indexes in arrays.</li>
    </ul>
</asp:Content>
<asp:Content ContentPlaceHolderID="CodePart1" runat="server">
    <h2>Code Snippet</h2>
    <p><strong>Code to create object</strong></p>
    <p style="font-family: 'PT Serif', serif;">
        var person = {<br />
            firstName:"John",<br />
            lastName:"Doe",<br />
            age:50,<br />
            eyeColor:"blue"<br />
        };<br />
   </p>
</asp:Content>
<asp:Content ContentPlaceHolderID="CodePart2" runat="server">
<h2>Code Example</h2>
    <p><strong>Try and Catch</strong></p>
    <p style="font-family: 'PT Serif', serif;">
        try {<br />
             Block of code to try<br />
            }<br />
        catch(err) {<br />
             Block of code to handle errors<br />
            }<br />
    </p>
</asp:Content>
<asp:Content ContentPlaceHolderID="CodePart3" runat="server">
    <h2>Useful Links</h2>
    <div>
        <ul class="links">
            <li><a href="https://modernweb.com/45-useful-javascript-tips-tricks-and-best-practices/">Tips and Tricks</a></li>
            <li><a href="https://www.w3schools.com/js/">Self Study</a></li>
        </ul>
    </div>
</asp:Content>