﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="portfolio._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>ASP.NET</h1>
        <p class="lead">This is a ASP.NET assignment. It is developed to practice all the lessons teached in class. This web application can also be used to learn Web application development, Database design and Javascript.</p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
    </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="CodePart1" runat="server">
            <h2>Database Design</h2>
            <p>
                Database Design part contains Tips to develope best database design, commands used in oracle sql database and some useful links.
            </p>
            <p>
                <a class="btn btn-default" href="/DatabaseDesign.aspx">Learn more &raquo;</a>
            </p>

</asp:Content>
<asp:Content ContentPlaceHolderID="CodePart2" runat="server">
            <h2>Web Application Development</h2>
            <p>
                This Section contains important points about ASP.NET, useful links and code snippets.
            </p>
            <p>
                <a class="btn btn-default" href="/WebAppDevelopment.aspx">Learn more &raquo;</a>
            </p>
</asp:Content>


<asp:Content ContentPlaceHolderID="CodePart3" runat="server">
            <h2>Javascript</h2>
            <p>
                Javascript is client side language and plays an important role in web application development.
            </p>
            <p>
                <a class="btn btn-default" href="/JavaScript.aspx">Learn more &raquo;</a>
            </p>
 </asp:Content>